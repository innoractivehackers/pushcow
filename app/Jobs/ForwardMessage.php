<?php

namespace App\Jobs;

use App\Message;
use App\Services\PushNotificationService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Throwable;

class ForwardMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected Message $message)
    {
        $this->onQueue('forward-message');

        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $service = app(PushNotificationService::class, ['device' => $this->message->device]);

        try {
            $service->push($this->message);

            $this->message->status = Message::STATUS_SUCCESS;
        } catch (Throwable $throwable) {
            Log::channel('debug')->info(get_class(), [
                'message' => $throwable->getMessage(),
                'exception' => get_class($throwable),
                'file' => $throwable->getFile(),
                'line' => $throwable->getLine(),
                'trace' => collect($throwable->getTrace())->map(fn ($trace) => Arr::except($trace, ['args']))->all(),
            ]);

            $this->message->status = Message::STATUS_FAILED;
        }

        $this->message->save();
    }
}
