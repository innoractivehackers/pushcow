<?php

namespace App\Http\Requests\V3;

use App\Http\Requests\V2\CreateMessageRequest as CreateMessageRequestV2;

class CreateMessageRequest extends CreateMessageRequestV2 {}
