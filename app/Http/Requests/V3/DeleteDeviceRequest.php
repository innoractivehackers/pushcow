<?php

namespace App\Http\Requests\V3;

use App\Http\Requests\V2\DeleteDeviceRequest as V2;

class DeleteDeviceRequest extends V2 {}
