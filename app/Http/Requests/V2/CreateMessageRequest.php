<?php

namespace App\Http\Requests\V2;

use App\Http\Requests\V1\CreateMessageRequest as CreateMessageRequestV1;

class CreateMessageRequest extends CreateMessageRequestV1 {}
