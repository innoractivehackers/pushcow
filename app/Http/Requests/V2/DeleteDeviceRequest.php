<?php

namespace App\Http\Requests\V2;

use App\Http\Requests\V1\DeleteDeviceRequest as V1;

class DeleteDeviceRequest extends V1 {}
